from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import *
from .serializers import *
from rest_framework import viewsets
from django.shortcuts import get_object_or_404

# Create your views here.
class PrilView(APIView):
    def get(self,request):
        prils=Pril.objects.all()
        serializer=PrilSerializer(prils,many=True)
        return Response({"books": serializer.data})

    def post(self,request):
        pril=request.data.get('prils')
        serializer=PrilSerializer(data=pril)
        if serializer.is_valid(raise_exception=True):
            prils_saved=serializer.save()
            return Response({"OK":"vse ok"})

class PrilViewSet(viewsets.ModelViewSet):
    serializer_class=PrilSerializer
    queryset=Pril.objects.all()


# class PrilViewSet(viewsets.ViewSet):
#     def list(self,request):
#         queryset=Pril.objects.all()
#         serializer=PrilSerializer(queryset,many=True)
#         return Response( serializer.data)
#
#     def retrieve(self,request,pk=None):
#         queryset=Pril.objects.all()
#         pril=get_object_or_404(queryset,pk=pk)
#         serializer=PrilSerializer(pril)
#         return Response( serializer.data)
