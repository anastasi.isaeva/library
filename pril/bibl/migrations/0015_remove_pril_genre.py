# Generated by Django 3.2.14 on 2022-07-18 16:17

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bibl', '0014_auto_20220718_1757'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pril',
            name='genre',
        ),
    ]
