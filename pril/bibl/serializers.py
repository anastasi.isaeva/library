from rest_framework import serializers
from .models import Pril, Author,Genre
#from .models import Pril

class PrilSerializer(serializers.ModelSerializer):
    class Meta:
        model=Pril
        fields=('id','title','text','created_date','publisher',"author","genre")
# class PrilSerializer(serializers.Serializer):
#     title=serializers.CharField(max_length=100)
#     text=serializers.CharField(max_length=400)
#     author_id=serializers.CharField(max_length=30)
#     genre_id=serializers.CharField(max_length=40)
#
#     def create(self,validated_data):
#         return Pril.objects.create(**validated_data)
