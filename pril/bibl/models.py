from django.db import models
from django.utils import timezone

# Create your models here.
class Pril(models.Model):
    title=models.CharField(verbose_name='Название книги',max_length=100)
    text=models.TextField(verbose_name='Текст книги')
    author=models.ForeignKey('Author',on_delete=models.CASCADE)
    genre=models.ForeignKey('Genre',on_delete=models.CASCADE)
    created_date=models.DateTimeField(default=timezone.now)
    publisher=models.CharField(verbose_name='Издательство',max_length=100,default='Художественная литература')
    def __str__(self):
        return self.title
    def get_author_gender(self):
        return self.author.gender

class Author(models.Model):
    MAN='MN'
    WOMAN='WN'
    FIO=models.CharField(verbose_name='ФИО автора',max_length=30,primary_key=True)
    text=models.TextField(verbose_name='Об авторе')
    gender=models.CharField(verbose_name='Пол',choices=[(MAN,'муж'),(WOMAN,'жен')],max_length=20,default='муж')
    def __str__(self):
        return self.FIO

class Genre(models.Model):
    title=models.CharField(verbose_name='Название жанра',max_length=40,primary_key=True)
    #text=models.TextField(verbose_name='Описание жанра')
    def __str__(self):
        return self.title
